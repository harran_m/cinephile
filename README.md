steps to run the app

* an optional docker development environment (laradock) is included, please refer for to https://laradock.io/getting-started/ for more info 

- to configure the app `.env.example to` `.env` and set mysql connection information
- to create the database tables run `php artisan migrate`
- to seed the database with dummy data run `php artisan db:seed`
- the endpoints are 
	- GET /movies      //list of movies
	- GET/movies/:id   // one movie details 
- the endpoints are protected by the token below
	- token: `LSZx83Hna5v0VVSOM42BLMDxbS0UQ2n0CLqRCzyVnneZzyRKa2X66FOGkV5ZKKYhgZ19Panr73lyU4VApTlNFlNR0278C6MyaG2c`
	-it should be provided as the parameter `apiKey` in the query 
- to search movies by a given title provide it as the parameter `searchQuery` in the query to the endpoint /movies
