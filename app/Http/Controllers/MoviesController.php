<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MoviesRepository;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\MovieTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class MoviesController extends Controller
{
    /**
     * $moviesRepo 
     * @var App\Transformers\MovieTransformer
     */
    protected $moviesRepo;

    /**
     * $transformManager transform Manager
     * @var League\Fractal\Manager
     */
    protected $transformManager;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MoviesRepository $moviesRepo, 
                                Manager $transformManager)
    {
        $this->moviesRepo = $moviesRepo;
        $this->transformManager = $transformManager;

        $this->middleware('auth');
    }

    /**
     * list list and search movies
     * @param  Request $request 
     */
    public function list(Request $request)
    {
        $searchQuery = $request->get('searchQuery');
        if(is_null($searchQuery)) {
            $moviesPaginator = $this->moviesRepo->get();
        } else {
            $moviesPaginator = $this->moviesRepo->search($searchQuery);
        }
        $movies = $moviesPaginator->getCollection();
        $resource = new Collection($movies, new MovieTransformer());
        $resource->setPaginator(new IlluminatePaginatorAdapter($moviesPaginator));

        return $this->transformManager->createData($resource)->toJson();
    }

    /**
     * show one movie details
     * @param  Request $request
     * @param  int  $id       id of movie
     */
    public function show(Request $request, $id)
    {  
        $movie = $this->moviesRepo->getById($id);
        $resource = new \League\Fractal\Resource\Item($movie, new MovieTransformer);

        return $this->transformManager->createData($resource)->toJson();
    }
}
