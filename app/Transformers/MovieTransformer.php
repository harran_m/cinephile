<?php 

namespace App\Transformers;

use App\Movie;
use League\Fractal\TransformerAbstract;

class MovieTransformer extends TransformerAbstract
{
	/**
	 * transform move object
	 * @param  Movie  $movie movie
	 * @return array       
	 */
    public function transform(Movie $movie)
    {
        return [
            'title' => $movie->title,
            'description' => $movie->description,
            'release_year' => $movie->release_year,
            'imdb_rating' => $movie->imdb_rating,
            'poster_image_url' => $movie->poster_image_url
        ];
    }
}
