<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
Use App\Movie;


class MoviesRepository extends Model
{
	/**
	 * [$itemsPerPage description]
	 * @var [type]
	 */
	protected $itemsPerPage;

	/**
	 * [__construct description]
	 */
	public function __construct()
    {
        $this->itemsPerPage = env('ITEMS_PER_PAGE', 10);
    }

	/**
	 * get list movies
	 * @param  [type] $itemsPerPage
	 * @return Collection               
	 */
    function get() 
    {
    	return Movie::paginate($this->itemsPerPage);
    }

    /**
     * search movies by string
     * @return Collection 
     */
    function search($searchQuery) 
    {
    	return Movie::where('title', 'like', "%{$searchQuery}%")->paginate($this->itemsPerPage);
    }

    /**
     * getById get a movie by id
     * @param  [type] $id movie id
     * @return      Movie
     */
    function getById($id) 
    {
    	return Movie::findOrFail($id);
    }
}
