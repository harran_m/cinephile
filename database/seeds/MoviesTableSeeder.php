<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        Movie::truncate();

        for ($i=0; $i < 20; $i++) {
            $movie = new Movie;
            $movie->title = $faker->sentence(4);
            $movie->description = $faker->paragraph;
            $movie->release_year = $faker->year;
            $movie->imdb_rating = $faker->randomFloat($nbMaxDecimals = 1, $min = 4, $max = 9.9);
            $movie->poster_image_url = $faker->imageUrl($width = 480, $height = 640);
            $movie->save();
        }
    }
}
