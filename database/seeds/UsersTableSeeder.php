<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        User::truncate();

        $user = new User;
        $user->name = $faker->name;
        $user->email = $faker->email;
        $user->api_key = 'LSZx83Hna5v0VVSOM42BLMDxbS0UQ2n0CLqRCzyVnneZzyRKa2X66FOGkV5ZKKYhgZ19Panr73lyU4VApTlNFlNR0278C6MyaG2c';

        $user->save();
    }
}
